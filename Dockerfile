FROM ubuntu:14.04
RUN mkdir -p /home/app
RUN apt-get update && apt-get install -y curl &&  apt-get install -y nodejs && ln -s /usr/bin/nodejs /usr/bin/node && apt-get install -y npm && npm install -g n && n 6.1.0
WORKDIR /home/app
ENV PORT=9000
EXPOSE 9000
CMD npm start

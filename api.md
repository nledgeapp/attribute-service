# Questionnaire API

## GET /api?id=xxx&name=xxx&type=xxx&source=xxx&group=xxx

###Response:
```
[
    {
        _id: String,
        name: String,
        type: String,
        group: String,
        normalized: {
            name: String,
            type: String,
            sources: [String],
            group: String
        },
        __v: Int,
        sources: [String]
    }
]
```

## POST /api 

### Parameters
```
{
    name: String,
    type: String,
    source: [String],
    group: String
}
```

'use strict';
import mongoose from 'mongoose';

/*********************************
  Add development specific config
  This will override the common/all one
**********************************/
export default {
    mysql: {
      host: 'localhost',
      port: 3306,
      user: 'root',
      password: 'root',
      database: 'attributes'
    }
};

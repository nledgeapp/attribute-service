/**
  Root endpoint, registers babel and runs app
*/
'use strict';

var server = require('./server');
module.exports = server();

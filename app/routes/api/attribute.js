'use strict';

import express from 'express';
import attribute from './attribute';

export default (app, dbService)=> {
  var router = express.Router();

  router.post('/attribute/', (req, res) => {
    let { body } = req;

    dbService.insertAttribute(body).then((id) => {
      res.status(200);
      res.set('Location', req.get('host') + '/api/attribute/' + id);
      res.send();

    }).catch((err) => {
      res.status(err.status ? err.status : 500);
      res.json(err);

    });
  });

  router.get('/attribute/', (req, res)=> {
    let query = req.query;

    dbService.findAllAttributes(query).then((attributes) => {
      res.json(attributes);

    }).catch((err) => {
      res.status(err.status ? err.status : 500);
      res.json(err);
    });
  });

  router.get('/attribute/:id', (req, res)=> {
    let { id } = req.params;

    dbService.findAttributeById(id).then((attribute) => {
      res.json(attribute);

    }).catch((err) => {
      res.status(err.status ? err.status : 500);
      res.json(err);
    });
  });

  return router;
}

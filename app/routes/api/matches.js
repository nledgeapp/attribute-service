'use strict';

import express from 'express';
import attribute from './attribute';

export default (app, dbService)=> {
  var router = express.Router();

  router.get('/matches/:userId', (req, res)=> {
    let { userId } = req.params;

    dbService.findMatches(userId).then((matches) => {
      res.json(matches);

    }).catch((err) => {
      res.status(err.status ? err.status : 500);
      res.json(err);
    });
  });

  router.get('/matches/:userId/details/:otherUserId', (req, res)=> {
    let { userId, otherUserId } = req.params;

    dbService.findMatchesWithDetails(userId, otherUserId).then((matches) => {
      res.json(matches);

    }).catch((err) => {
      res.status(err.status ? err.status : 500);
      res.json(err);
    });
  });

  return router;
}

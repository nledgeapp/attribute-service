'use strict';

import express from 'express';
import attribute from './attribute';

export default (app, dbService)=> {
  var router = express.Router();

  router.get('/user-attribute', (req, res)=> {
    let query = req.query;

    dbService.findAllUserAttributes(query).then((userAttributes) => {
      res.json(userAttributes);

    }).catch((err) => {
      res.status(err.status ? err.status : 500);
      res.json(err);
    });
  });

  router.post('/user-attribute', (req, res) => {
    let { body } = req;

    dbService.insertUserAttribute(body).then((id) => {
      res.status(204);
      res.send();

    }).catch((err) => {
      res.status(err.status ? err.status : 500);
      res.json(err);

    });
  });

  return router;
}

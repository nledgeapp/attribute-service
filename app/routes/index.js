'use strict';

import attributeApi from './api/attribute';
import userAttributeApi from './api/userAttribute';
import matchesApi from './api/matches';
import errors from './errors.route.js';

export default (app, dbService)=> {
    // API routes
    app.use('/', attributeApi(app, dbService));
    app.use('/', userAttributeApi(app, dbService));
    app.use('/', matchesApi(app, dbService));

    app.get('/', (req, res)=> {
        res.json({
          "api": {
            "GET  /attribute": "Get all attributes with possible query parameters (name)",
            "GET  /attribute/:id": "Get one attribute by id",
            "POST /attribute": "Create new attributes. Payload: { \"name\": \"YOUR NAME HERE\"}",
            "GET  /user-attributes?user_id=1": "Get all attributes for user with id: 1",
            "GET  /user-attributes?name=Java": "Get all users that have the attribute 'Java'"
          }
        });
    });
    // Add a not found route
    app.use(errors.notFound);

    // Lastly add a error route
    app.use(errors.error);
}

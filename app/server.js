'use strict';

import express from 'express';
import expressConfigure from './express/express';
import config from './config';
import routes from './routes';
import DatabaseService from './services/dbservice'

function server() {
    /********************************
    Create and configure express app
    ********************************/
    var app = express();
    expressConfigure(app);

    let dbService = new DatabaseService();

    routes(app, dbService);

    /********************************
    Start server
    ********************************/
    app.listen(config.PORT, () => {
        console.log([
            'Running Server',
            'Project: %s',
            'Mode: %s',
            'Port: %s'
        ].join('\n'), config.projectName, config.NODE_ENV, config.PORT);
    });
    return app;
}

/********************************
  Expose app
********************************/
export default server;

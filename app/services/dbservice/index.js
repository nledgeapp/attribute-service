import path from 'path'
import mysql from 'mysql';

let root = path.dirname(require.main.filename),
    config = require(path.normalize(root+'/config'));

export default function() {
    var pool = mysql.createPool(config.mysql);

    function connectDb() {
      return new Promise((resolve, reject)=>{
        resolve();
      });
    }

    function findMatchesWithDetails(userId, otherUserId) {
      return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
          if (err) {
            console.log(err);
            reject(err);
          }
          let sql = `
              SELECT
                  (mine.weight) AS my_total_weight,
                  others.user_id As matched_user_id,
                  (others.weight) AS matched_user_total_weight,
                  (CAST(mine.weight AS SIGNED) - CAST(mine.weight AS SIGNED) * 0.20 * ABS(CAST(mine.weight AS SIGNED) - CAST(others.weight AS SIGNED))) AS Score
              FROM user_attributes AS mine
                  JOIN attributes ON mine.attribute_id = attributes.id
                  JOIN user_attributes AS others ON mine.attribute_id = others.attribute_id
              WHERE
                  mine.user_id <> others.user_id
                  AND mine.user_id = ? AND others.user_id = ?
              ORDER BY Score DESC
          `;

          connection.query(sql, [userId, otherUserId], (err, results) => {
            connection.release();
            if (err) {
              return reject(err);
            }
            resolve(results);
          });
        });
      });
    }

    function findMatches(userId) {
      return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
          if (err) {
            console.log(err);
            reject(err);
          }
          let sql = `
              SELECT
                  SUM(mine.weight) AS my_total_weight,
                  others.user_id As matched_user_id,
                  SUM(others.weight) AS matched_user_total_weight,
                  COUNT(*) AS matched_attributes,
                  SUM(CAST(mine.weight AS SIGNED) - CAST(mine.weight AS SIGNED) * 0.25 *ABS(CAST(mine.weight AS SIGNED) - CAST(others.weight AS SIGNED)))/COUNT(*) AS score
              FROM user_attributes AS mine
                  JOIN attributes ON mine.attribute_id = attributes.id
                  JOIN user_attributes AS others ON mine.attribute_id = others.attribute_id
              WHERE
                  mine.user_id <> others.user_id
                  AND mine.user_id = ?
              GROUP BY others.user_id
              ORDER BY Score DESC
          `;

          connection.query(sql, [userId], (err, results) => {
            connection.release();
            if (err) {
              return reject(err);
            }
            resolve(results);
          });
        });
      });
    }

    function insertUserAttribute(payload) {
      return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
          if (err) {
            console.log(err);
            reject(err);
          }
          if (!payload.user_id || typeof(payload.user_id) !== "number" || payload.user_id <= 0) {
            return reject({status: 422, error: "Property 'user_id' is mandatory and must be an integer greater than 0."});
          }

          if (!payload.attribute_id || typeof(payload.attribute_id) !== "number" || payload.attribute_id <= 0) {
            return reject({status: 422, error: "Property 'attribute_id' is mandatory and must be an integer greater than 0."});
          }

          if (!payload.weight || typeof(payload.weight) !== "number" || !( 1 <= payload.weight <= 5) ) {
            return reject({status: 422, error: "Property 'weight' is mandatory and must be an integer between 1 and 5."});
          }

          let query = "INSERT INTO user_attributes SET ?";
          connection.query(query, payload, function(err, result) {
            connection.release();
            if (err) {
              if (err.code === 'ER_DUP_ENTRY') {
                reject({status: 409, error: "The combination user_id and attribute_id ("+payload.user_id+", "+payload.attribute_id+") already exists."});
              }

              return reject(err);
            }

            resolve();
          });
        });
      });
    }

    function findAllUserAttributes(query) {
      return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
          if (err) {
            console.log(err);
            reject(err);
          }
          var whereClauses = [];
          if (query.user_id) {
            whereClauses[whereClauses.length] = 'user_id = ' + connection.escape(query.user_id);
            delete query.user_id;
          }

          if (query.name) {
            whereClauses[whereClauses.length] = 'name = ' + connection.escape(query.name);
            delete query.name;
          }

          let remainingKeys = Object.keys(query);
          if (remainingKeys.length !== 0) {
            reject({
              status: 422,
              error: "Unsupported filters: " + remainingKeys.join(", ")
            });
          }

          var sql = "SELECT ua.id, ua.user_id, ua.attribute_id, a.name, ua.weight FROM user_attributes AS ua INNER JOIN attributes AS a ON a.id = ua.attribute_id";
          if (whereClauses.length == 0) {
            reject({
              status: 422,
              error: "At least one filter of 'user_id' or 'name' must be supplied"
            });
          }

          sql += " WHERE " + whereClauses.join(" AND ");

          connection.query(sql, (err, results) => {
            connection.release();
            if (err) {
              return reject(err);
            }
            resolve(results);
          });
        });
      });
    }

    function insertAttribute(payload) {
      return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
          if (err) {
            console.log(err);
            reject(err);
          }

          if (!payload.name || typeof(payload.name) !== "string" || payload.name.length > 255) {
            return reject({status: 422, error: "Property 'name' is mandatory and must be of type string of at most 255 characters"});
          }

          connection.query("INSERT INTO attributes (name) VALUES (?)", [payload.name], function(err, result) {
            connection.release();
            if (err) {
              if (err.code === 'ER_DUP_ENTRY') {
                return reject({status: 409, error: "The attribute with name '" + payload.name + "' already exists."});
              }

              return reject(err);
            }

            resolve(result.insertId);
          });
        });
      });
    }

    function findAttributeById(id) {
      return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
          if (err) {
            console.log(err);
            reject(err);
          }

          connection.query("SELECT * FROM attributes WHERE id = ?", [id], function(err, result) {
            connection.release();
            if (err) {
              return reject(err);
            }
            if (result.length !== 1) {
              return reject({status:404, error: "Resource not found"});
            }

            resolve(result[0]);
          });
        });
      });
    }

    function findAllAttributes(query) {
      return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
          if (err) {
            console.log(err);
            reject(err);
          }

          var whereClauses = [];
          if (query.name) {
            whereClauses = [
              'name = ' + connection.escape(query.name)
            ];
            delete query.name;
          }

          let remainingKeys = Object.keys(query);
          if (remainingKeys.length !== 0) {
            reject({
              status: 422,
              error: "Unsupported filters: " + remainingKeys.join(", ")
            });
          }

          var sql = "SELECT * FROM attributes";
          if (whereClauses.length > 0) {
            sql += " WHERE " + whereClauses.join(" AND ");
          }

          connection.query(sql, (err, results) => {
            connection.release();
            if (err) {
              return reject(err);
            }
            resolve(results);
          });
        });
      });
    }

    return {
      connectDb,
      findAttributeById,
      findAllAttributes,
      insertAttribute,
      findAllUserAttributes,
      insertUserAttribute,
      findMatches,
      findMatchesWithDetails
    };
}
